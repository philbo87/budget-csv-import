package com.philbo87.budget.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.yaml.snakeyaml.Yaml;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public class ConfigurationModule extends AbstractModule {

	private String configurationFilePath;
	private Configuration configuration;
	
	public ConfigurationModule(String configurationFilePath){
		this.configurationFilePath = configurationFilePath;
	}
	
	@Provides
	public Configuration getConfiguration() throws IOException {
		if(configuration == null){
			configuration = parseConfigurationFile();
		}
		
		return configuration;
	}
	
	public Configuration parseConfigurationFile() throws IOException {
		try(InputStream in = Files.newInputStream(Paths.get(this.configurationFilePath))){
			Configuration configuration = new Yaml().loadAs(in, Configuration.class);
			return configuration;
		}
	}

	@Override
	protected void configure() {}
}
