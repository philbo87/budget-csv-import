package com.philbo87.budget.configuration;

public class Configuration {
	private String amexSheetPath;
	private String landmarkSheetPath;
	private String landmarkVisaSheetPath;
	private String budgetSpreadsheetId;
	
	public String getAmexSheetPath() {
		return amexSheetPath;
	}
	public void setAmexSheetPath(String amexSheetPath) {
		this.amexSheetPath = amexSheetPath;
	}
	public String getLandmarkSheetPath() {
		return landmarkSheetPath;
	}
	public void setLandmarkSheetPath(String landmarkSheetPath) {
		this.landmarkSheetPath = landmarkSheetPath;
	}
	public String getLandmarkVisaSheetPath() {
		return landmarkVisaSheetPath;
	}
	public void setLandmarkVisaSheetPath(String landmarkVisaSheetPath) {
		this.landmarkVisaSheetPath = landmarkVisaSheetPath;
	}
	public String getBudgetSpreadsheetId() {
		return budgetSpreadsheetId;
	}
	public void setBudgetSpreadsheetId(String budgetSpreadsheetId) {
		this.budgetSpreadsheetId = budgetSpreadsheetId;
	}
	
}
