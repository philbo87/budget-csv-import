package com.philbo87.budget.internal.models;

public enum ECategory {
	JointAccount("Joint Account",1), 
	InsuranceAndCarFees("Insurances & Car Fees",2), 
	CarExpenses("Car Expenses",3), 
	CarGas("Car Gas",4), 
	FunMoney("Fun Money",5), 
	InternetAccounts("Internet Accounts",6), 
	Vacation("Vacation",7), 
	Misc("Misc",8), 
	Christmas("Christmas",9), 
	HouseFund("House Fund",10), 
	EmergencyFund("Emergency Fund",11);
	
	private String friendlyName;
	private int numericValue;

	private ECategory(String friendlyName, int numericValue){
		this.friendlyName = friendlyName;
		this.numericValue = numericValue;
	}
	
	public String getFriendlyName(){
		return friendlyName;
	}

	public int getNumericValue() {
		return numericValue;
	}
}
