package com.philbo87.budget.internal.models;

public enum ESource {
	AmericanExpress("American Express", "AMEX Payoff"), LandmarkVisa("Landmark Visa", "Visa Payoff"), Landmark("Landmark Credit Union");

	private String friendlyName;
	private String payoffSheetName;
	
	private ESource(String friendlyName, String payoffSheetName){
		this.friendlyName = friendlyName;
		this.payoffSheetName = payoffSheetName;
	}
	
	private ESource(String friendlyName){
		this.friendlyName = friendlyName;
	}
	
	public String getFriendlyName(){
		return friendlyName;
	}

	public String getPayoffSheetName() {
		return payoffSheetName;
	}
}
