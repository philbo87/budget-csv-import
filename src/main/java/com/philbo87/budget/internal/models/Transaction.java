package com.philbo87.budget.internal.models;

import org.joda.time.LocalDate;

public class Transaction implements Comparable<Transaction> {
	public Transaction(LocalDate date, String description, double amount, ESource creditCard) {
		this.date = date;
		this.description = description;
		this.amount = amount;
		this.transactionSource = creditCard;
	}
	
	private LocalDate date;
	private String description;
	private double amount;
	private ESource transactionSource;
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public ESource getTransactionSource() {
		return transactionSource;
	}
	public void setTransactionSource(ESource creditCard) {
		this.transactionSource = creditCard;
	}
	
	public int compareTo(Transaction t) {
		return getDate().compareTo(t.getDate());
	}
}
