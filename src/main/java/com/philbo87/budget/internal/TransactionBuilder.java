package com.philbo87.budget.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.philbo87.budget.internal.models.ESource;
import com.philbo87.budget.internal.models.Transaction;
import com.philbo87.budget.thirdparty.datamodel.AmericanExpressRow;
import com.philbo87.budget.thirdparty.datamodel.LandmarkRow;
import com.philbo87.budget.thirdparty.datamodel.LandmarkVisaRow;

/**
 * Builds a List<Transaction> from third party rows sorted by date.
 *
 */
public class TransactionBuilder {

	private List<AmericanExpressRow> amexRows;
	private List<LandmarkRow> landmarkRows;
	private List<LandmarkVisaRow> landmarkVisaRows;

	public TransactionBuilder(List<AmericanExpressRow> amexRows, List<LandmarkRow> landmarkRows,
			List<LandmarkVisaRow> landmarkVisaRows) {
		this.amexRows = amexRows;
		this.landmarkRows = landmarkRows;
		this.landmarkVisaRows = landmarkVisaRows;
	}

	public List<Transaction> build() {
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions = transformAmericanExpressRowsToTransactions(transactions);
		transactions = transformLandmarkRowsToTransactions(transactions);
		transactions = transformLandmarkVisaRowsToTransactions(transactions);

		// Sorts transactions by date
		Collections.sort(transactions);

		return transactions;
	}

	private List<Transaction> transformLandmarkVisaRowsToTransactions(List<Transaction> transactions) {
		for (LandmarkVisaRow landmarkVisaRow : landmarkVisaRows) {
			transactions.add(new Transaction(landmarkVisaRow.getDate(), landmarkVisaRow.getMerchantName(),
					-landmarkVisaRow.getAmount(), ESource.LandmarkVisa));
		}

		return transactions;
	}

	private List<Transaction> transformLandmarkRowsToTransactions(List<Transaction> transactions) {
		for (AmericanExpressRow amexRow : amexRows) {
			transactions.add(new Transaction(amexRow.getDate(), amexRow.getShortName(), -amexRow.getAmount(),
					ESource.AmericanExpress));
		}

		return transactions;
	}

	private List<Transaction> transformAmericanExpressRowsToTransactions(List<Transaction> transactions) {
		for (LandmarkRow landmarkRow : landmarkRows) {
			transactions.add(new Transaction(landmarkRow.getDate(), landmarkRow.getMemo(), landmarkRow.getAmount(),
					ESource.Landmark));
		}

		return transactions;
	}
}
