package com.philbo87.budget.internal;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.google.inject.Inject;
import com.philbo87.budget.dao.ITransactionDAO;
import com.philbo87.budget.internal.models.ECategory;
import com.philbo87.budget.internal.models.Transaction;

public class TransactionInserter implements ITransactionInserter {

	private ITransactionDAO transactionDAO;
	private final List<ECategory> CATEGORIES = Arrays.asList(ECategory.values());

	@Inject
	private TransactionInserter(ITransactionDAO transactionDAO) {
		this.transactionDAO = transactionDAO;
	}

	public void askIfTransactionsShouldBeInserted(List<Transaction> transactions) {
		try (Scanner reader = new Scanner(System.in)) {
			for (Transaction transaction : transactions) {
				System.out.println(
						String.format("Transaction:\n\tDate: %s\n\tDescription: %s\n\tAmount: %s\n\tSource: %s",
								transaction.getDate(), transaction.getDescription(), transaction.getAmount(),
								transaction.getTransactionSource().getFriendlyName()));

				System.out.println("Do you want to insert this transaction? (Y)es or (N)o");

				String input = reader.next();
				input = input.toLowerCase();

				if (input.charAt(0) == 'y') {
					printCategorySelection();

					int selectedCategoryNumericValue = reader.nextInt();
					Optional<ECategory> selectedCategory = CATEGORIES.stream()
							.filter(x -> x.getNumericValue() == (selectedCategoryNumericValue)).findAny();
					
					transactionDAO.save(transaction, selectedCategory.get());
				}
			}
		}
	}

	private void printCategorySelection() {
		System.out.println("Choose category by number: ");
		for (ECategory category : CATEGORIES) {
			System.out.println(String.format("%s. %s", category.getNumericValue(), category.getFriendlyName()));
		}
	}
}
