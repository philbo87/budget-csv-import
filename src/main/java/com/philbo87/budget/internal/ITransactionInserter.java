package com.philbo87.budget.internal;

import java.util.List;

import com.philbo87.budget.internal.models.Transaction;

public interface ITransactionInserter {
	public void askIfTransactionsShouldBeInserted(List<Transaction> transactions);
}
