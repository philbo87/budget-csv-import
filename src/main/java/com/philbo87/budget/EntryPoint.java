package com.philbo87.budget;

import java.util.List;
import com.google.inject.Injector;
import com.philbo87.budget.configuration.Configuration;
import com.philbo87.budget.internal.ITransactionInserter;
import com.philbo87.budget.internal.TransactionBuilder;
import com.philbo87.budget.thirdparty.datamodel.AmericanExpressRow;
import com.philbo87.budget.thirdparty.datamodel.LandmarkRow;
import com.philbo87.budget.thirdparty.datamodel.LandmarkVisaRow;
import com.philbo87.budget.thirdparty.parser.AmericanExpressParser;
import com.philbo87.budget.thirdparty.parser.LandmarkParser;
import com.philbo87.budget.thirdparty.parser.LandmarkVisaParser;

public class EntryPoint {

	public static void main(String[] args) throws Exception {
		validateArguments(args);		
		Injector injector = GuiceInjectorHook.createBindings(args[0]);
		
		Configuration configuration = injector.getInstance(Configuration.class);
		String amexSheetPath = configuration.getAmexSheetPath();
		String landmarkSheetPath = configuration.getLandmarkSheetPath();
		String landmarkVisaSheetPath = configuration.getLandmarkVisaSheetPath();

		System.out.println("Parsing American Express sheet: " + amexSheetPath);
		List<AmericanExpressRow> amexRows = AmericanExpressParser.parseCsvFile(amexSheetPath);
		System.out.println("Done parsing American Express sheet. #Items: " + amexRows.size());

		System.out.println("Parsing Landmark sheet: " + landmarkSheetPath);
		List<LandmarkRow> landmarkRows = LandmarkParser.parseCsvFile(landmarkSheetPath);
		System.out.println("Done parsing Landmark sheet. #Items: " + landmarkRows.size());

		System.out.println("Parsing Landmark Visa sheet: " + landmarkVisaSheetPath);
		List<LandmarkVisaRow> landmarkVisaRows = LandmarkVisaParser.parseCsvFile(landmarkVisaSheetPath);
		System.out.println("Done parsing Landmark Visa sheet. #Items: " + landmarkVisaRows.size());

		TransactionBuilder builder = new TransactionBuilder(amexRows, landmarkRows, landmarkVisaRows);

		ITransactionInserter transactionInserter = injector.getInstance(ITransactionInserter.class);
		transactionInserter.askIfTransactionsShouldBeInserted(builder.build());
	}

	private static void validateArguments(String[] args) throws Exception {
		if (args.length != 1) {
			throw new IllegalArgumentException("Illegal arguments provided. One YAML configuration file is expected.");
		}
	}

}
