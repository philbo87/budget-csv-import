package com.philbo87.budget;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.philbo87.budget.configuration.ConfigurationModule;
import com.philbo87.budget.dao.ITransactionDAO;
import com.philbo87.budget.dao.google.GoogleSheetsTransactionDAO;
import com.philbo87.budget.internal.ITransactionInserter;
import com.philbo87.budget.internal.TransactionInserter;

public class GuiceInjectorHook {

	public static Injector createBindings(String configurationFilePath) {
		return Guice.createInjector(new ConfigurationModule(configurationFilePath), new AbstractModule() {
			@Override
			protected void configure() {
				bind(ITransactionDAO.class).to(GoogleSheetsTransactionDAO.class);
				bind(ITransactionInserter.class).to(TransactionInserter.class);
			}
		});
	}
}
