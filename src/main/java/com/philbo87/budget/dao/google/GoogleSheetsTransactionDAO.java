package com.philbo87.budget.dao.google;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.inject.Inject;
import com.philbo87.budget.configuration.Configuration;
import com.philbo87.budget.dao.ITransactionDAO;
import com.philbo87.budget.internal.models.ECategory;
import com.philbo87.budget.internal.models.ESource;
import com.philbo87.budget.internal.models.Transaction;

public class GoogleSheetsTransactionDAO implements ITransactionDAO {

	private Configuration configuration;

	@Inject
	private GoogleSheetsTransactionDAO(Configuration configuration) {
		this.configuration = configuration;
	}

	private static final String APPLICATION_NAME = GoogleSheetsTransactionDAO.class.getSimpleName();
	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"),
			".credentials/" + GoogleSheetsTransactionDAO.class.getName());
	private static FileDataStoreFactory DATA_STORE_FACTORY;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static HttpTransport HTTP_TRANSPORT;
	private static List<Sheet> sheetsInSpreadsheet;

	private static final List<String> SCOPES = Arrays.asList(SheetsScopes.SPREADSHEETS);
	private static final List<ESource> CREDIT_CARD_SOURCES = Arrays.asList(ESource.AmericanExpress,
			ESource.LandmarkVisa);

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	public void save(Transaction t, ECategory category) {
		try {
			if (sheetsInSpreadsheet == null) {
				saveCategorySheetsToMemeory();
			}

			Sheets service = getSheetsService();
			String spreadsheetId = configuration.getBudgetSpreadsheetId();

			Optional<Sheet> sheetForCategory = sheetsInSpreadsheet.stream()
					.filter(x -> category.getFriendlyName().equals(x.getProperties().getTitle())).findAny();

			ValueRange requestBody = buildRequestBody(t);
			service.spreadsheets().values()
					.append(spreadsheetId, sheetForCategory.get().getProperties().getTitle(), requestBody)
					.setValueInputOption("USER_ENTERED").execute();

			if (CREDIT_CARD_SOURCES.contains(t.getTransactionSource())) {
				ValueRange requestBodyCreditCard = buildRequestBodyForCreditCardTransaction(t);
				service.spreadsheets().values()
						.append(spreadsheetId, t.getTransactionSource().getPayoffSheetName(), requestBodyCreditCard)
						.setValueInputOption("USER_ENTERED").execute();
			}
		} catch (IOException e) {
			// TODO this is bad and I feel bad
			System.out.println(e.getMessage() + e.getStackTrace());
		}
	}

	private ValueRange buildRequestBodyForCreditCardTransaction(Transaction t) {
		List<List<Object>> rowToInsert = Arrays
				.asList(Arrays.asList(t.getDate().toString(), t.getDescription(), -t.getAmount()));
		return new ValueRange().setValues(rowToInsert);
	}

	private ValueRange buildRequestBody(Transaction t) {
		List<List<Object>> rowToInsert = null;

		// If this comes from a Credit Card Source, mark it as such for the
		// "Credit Card" column.
		if (CREDIT_CARD_SOURCES.contains(t.getTransactionSource())) {
			rowToInsert = Arrays.asList(Arrays.asList(t.getDate().toString(), t.getDescription(), t.getAmount(),
					t.getTransactionSource().getFriendlyName()));
		} else {
			rowToInsert = Arrays.asList(Arrays.asList(t.getDate().toString(), t.getDescription(), t.getAmount()));
		}
		
		return new ValueRange().setValues(rowToInsert);
	}

	private void saveCategorySheetsToMemeory() throws IOException {
		Sheets service = getSheetsService();
		String spreadsheetId = configuration.getBudgetSpreadsheetId();
		Spreadsheet s = service.spreadsheets().get(spreadsheetId).execute();
		sheetsInSpreadsheet = s.getSheets();
	}

	public static Sheets getSheetsService() throws IOException {
		Credential credential = authorize();
		return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
				.build();
	}

	private static Credential authorize() throws IOException {
		InputStream in = GoogleSheetsTransactionDAO.class.getResourceAsStream("/client_secret.json");
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES).setDataStoreFactory(DATA_STORE_FACTORY).setAccessType("offline").build();

		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
		return credential;
	}
}
