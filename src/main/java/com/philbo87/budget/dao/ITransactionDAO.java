package com.philbo87.budget.dao;

import com.philbo87.budget.internal.models.ECategory;
import com.philbo87.budget.internal.models.Transaction;

public interface ITransactionDAO {

	public void save(Transaction t, ECategory category);
}
