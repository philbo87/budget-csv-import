package com.philbo87.budget.thirdparty.parser;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import com.philbo87.budget.thirdparty.datamodel.LandmarkRow;

public class LandmarkParser {
	private static final int FIRST_DATA_ROW = 5;
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("MM/dd/yyyy");// Format 4/5/2017
	
	public static List<LandmarkRow> parseCsvFile(String fileName) {
		FileReader fileReader = null;
		CSVParser csvFileParser = null;
		List<LandmarkRow> transformedRows = new ArrayList<LandmarkRow>();

		try {
			fileReader = new FileReader(fileName);
			csvFileParser = new CSVParser(fileReader, CSVFormat.DEFAULT);

			for (CSVRecord record : csvFileParser.getRecords()) {
				if (record.getRecordNumber() >= FIRST_DATA_ROW) {
					transformedRows.add(transformCsvRowToLandmarkRow(record));
				}
			}
		} catch (Exception e) {
			System.out.println("LandmarkParser had an issue. " + e.getStackTrace());
		} finally {
			try {
				fileReader.close();
				csvFileParser.close();
			} catch (IOException e) {
				System.out.println("IOException in finally in LandmarkParser");
			}
		}
		return transformedRows;
	}

	private static LandmarkRow transformCsvRowToLandmarkRow(CSVRecord record) {
		LocalDate date = DATE_FORMATTER.parseLocalDate(record.get(1));
		String descritpion = record.get(2);
		String memo = record.get(3);
		double amount = getAmountFromCsvRow(record);
		double balance = Double.parseDouble(record.get(6));

		return new LandmarkRow(date, descritpion, memo, amount, balance);
	}

	private static double getAmountFromCsvRow(CSVRecord record) {
		double amount = 0;
		
		// The 4th column (0 indexed) is Amount Debit, the 5th column is Amount
		// Credit. We want whichever is not empty.
		if (record.get(4).isEmpty()) {
			amount = Double.parseDouble(record.get(5));
		} else {
			amount = Double.parseDouble(record.get(4));
		}

		return amount;
	}
}
