package com.philbo87.budget.thirdparty.parser;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.philbo87.budget.thirdparty.datamodel.AmericanExpressRow;

public class AmericanExpressParser {
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("MM/dd/yyyy");//Format 04/02/2017

	public static List<AmericanExpressRow> parseCsvFile(String fileName) {
		FileReader fileReader = null;
		CSVParser csvFileParser = null;
		List<AmericanExpressRow> transformedRows = new ArrayList<AmericanExpressRow>();

		try {
			fileReader = new FileReader(fileName);
			csvFileParser = new CSVParser(fileReader, CSVFormat.DEFAULT);

			for (CSVRecord record : csvFileParser.getRecords()) {
				transformedRows.add(transformCsvRowToAmericanExpressRow(record));
			}
			
		} catch (Exception e) {
			System.out.println("AmericanExpressParser had an issue. " + e.getStackTrace());
		} finally {
			try {
				fileReader.close();
				csvFileParser.close();
			} catch (IOException e) {
				System.out.println("IOException in finally in AmericanExpressParser");
			}
		}
		return transformedRows;
	}

	private static AmericanExpressRow transformCsvRowToAmericanExpressRow(CSVRecord record) {
		LocalDate date = DATE_FORMATTER.parseLocalDate(record.get(0).substring(0, 10));
		String longName = record.get(2);
		String shortName = record.get(11);
		double amount = Double.valueOf(record.get(7));

		return new AmericanExpressRow(date, shortName, longName, amount);
	}
}
