package com.philbo87.budget.thirdparty.parser;

import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.philbo87.budget.thirdparty.datamodel.LandmarkVisaRow;

public class LandmarkVisaParser {
	private static final int FIRST_DATA_ROW = 2;
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("MM/dd/yyyy");// Format 4/5/2017

	public static List<LandmarkVisaRow> parseCsvFile(String fileName) {
		FileReader fileReader = null;
		CSVParser csvFileParser = null;
		List<LandmarkVisaRow> transformedRows = new ArrayList<LandmarkVisaRow>(); 
		
		try {
			fileReader = new FileReader(fileName);
			csvFileParser = new CSVParser(fileReader, CSVFormat.DEFAULT);
			
			for (CSVRecord record : csvFileParser.getRecords()) {
				if (record.getRecordNumber() >= FIRST_DATA_ROW) {
					transformedRows.add(transformCsvRowToLandmarkVisaRow(record));
				}
			}
		} catch (Exception e){
			System.out.println("LandmarkVisaParser had an issue. " + e.getStackTrace());
		} finally {
			try {
				fileReader.close();
				csvFileParser.close();
			} catch (IOException e) {
				System.out.println("IOException in finally in LandmarkVisaParser");
			}
		}
		return transformedRows;
		
	}

	private static LandmarkVisaRow transformCsvRowToLandmarkVisaRow(CSVRecord record) throws ParseException {
		LocalDate date = DATE_FORMATTER.parseLocalDate(record.get(2));
		String merchantName = record.get(5);
		double amount = NumberFormat.getCurrencyInstance().parse(record.get(10)).doubleValue();

		
		return new LandmarkVisaRow(date, merchantName, amount);
	}
}
