package com.philbo87.budget.thirdparty.datamodel;

import org.joda.time.LocalDate;

public class LandmarkVisaRow {

	public LandmarkVisaRow(LocalDate date, String merchantName, double amount) {
		this.date = date;
		this.merchantName = merchantName;
		this.amount = amount;
	}
	
	private LocalDate date;
	private String merchantName;
	private double amount;
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
