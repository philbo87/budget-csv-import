package com.philbo87.budget.thirdparty.datamodel;

import org.joda.time.LocalDate;

public class LandmarkRow {
	
	public LandmarkRow(LocalDate date, String description, String memo, double amount, double balance) {
		this.date = date;
		this.description = description;
		this.memo = memo;
		this.amount = amount;
		this.balance = balance;
	}
	
	private LocalDate date;
	private String description;
	private String memo;
	private double amount;
	private double balance;
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
}
