package com.philbo87.budget.thirdparty.datamodel;

import org.joda.time.LocalDate;

public class AmericanExpressRow {

	private LocalDate date;
	private String shortName;
	private String longName;
	private double amount;
	
	public AmericanExpressRow(LocalDate date, String shortName, String longName, double amount){
		this.date = date;
		this.shortName = shortName;
		this.longName = longName;
		this.amount = amount;
	}
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getLongName() {
		return longName;
	}
	public void setLongName(String longName) {
		this.longName = longName;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
